# Context parameters

Context parameters are additional value pairs that are sent to your add-on in the request URL from the host application. Using context parameters, your add-ons can selectively alter their behaviour based on information provided by the application. Common examples include displaying alternate content, or even performing entirely different operations based on the available context.

On this page

* [Standard parameters](#standard-parameters)
* [Conditions in query parameters](#inline-conditions)
* [JIRA parameters](#additional-parameters-jira)
* [JIRA Service Desk parameters](#additional-parameters-jira-service-desk)
* [JIRA Software parameters](#additional-parameters-jira-software)
* [Add-on-specific context parameters](#add-on-specific-context-parameters)

## <a name="standard-parameters"></a>Standard parameters

The following parameters are common across all requests and are always included in the URL query string.

* `lic`: the [license status](https://developer.atlassian.com/static/connect/docs/latest/concepts/licensing.html#license-status)
* `loc`: the user's locale (eg: `en-GB`)
* `tz`: the user's timezone (eg: `Australia/Sydney`)
* `cp`: the context path of the instance (eg: `/wiki`)
* `xdm_e`: the base url of the host application, used for the Javascript bridge (xdm - cross domain messaging)
* `xdm_c`: the xdm channel to establish communication over
* `cv`: the version of Atlassian Connect that made the HTTP request to your add-on.

 <span class="aui-lozenge aui-lozenge-current">DEPRECATED</span>
These parameters are deprecated in favour of the [context.user JWT claim](../understanding-jwt.html#token-structure-claims):

* `user_id`: the user's username. This may change and `user_key` should be used instead
* `user_key`: the user's unique identifier



## <a name="additional-parameters"></a>Additional parameters

Your Atlassian Connect add-on can also request context parameters from the Atlassian application by using
variable tokens in the URL attributes of your modules. The add-on can use this to present content specific to the
context, for example, for the particular JIRA issue or project that is open in the browser. A particular variable is
available only where it makes sense given the application context.

URL variables are available to any of the page modules, including web panels, web items, general pages and dialog pages,
except for Confluence macros. To add a variable to a URL, enclose the variable name in curly brackets, as follows: `{variable.name}`.
The context variable must be either a path component or the `value` in a query string parameter formatted as `name=value`.

For example, the following URL includes variables that are bound to the JIRA project id and current issue key at runtime:

``` json
{
    "url": "/myPage/projects/{project.id}?issueKey={issue.key}"
}
```

Note that conventional URL encoding means that context parameters passed as a query parameter will be encoded
slightly differently from those included as a path component. The path component will use [percent encoding](https://en.wikipedia.org/wiki/Percent-encoding), while the query component will use [`application/x-www-form-urlencoded` encoding](http://www.w3.org/TR/html5/forms.html#application/x-www-form-urlencoded-encoding-algorithm). 
The primary difference is that a space in a query parameter will be encoded as a `+`, while in the path component it will be encoded as `%20`.

If the application isn't able to bind a value to the variable at runtime for any reason, it passes an empty value instead.

### <a name="inline-conditions"></a>Conditions in query parameters

[Conditions](../conditions/) are a powerful way to control whether your UI elements should be shown or not. 
However, you may want your element to always display but behave differently depending on user permissions or other states that the conditions allow you to check. 

You can achieve this goal by putting the conditions in query parameters. The condition will be dynamically
evaluated every time the URL is requested based on the current user and context.

The syntax of such variables is as follows (in the BNF notation with optional items enclosed in square brackets):

```
<variable>        ::=  "condition." <condition-name> [<parameters>]
<parameters>      ::= "(" [<parameters-list>] ")"
<parameters-list> ::=  <parameter-key> "=" <parameter-value> | <parameters-list> "," <parameters-list>

```

Any condition available in other places can be used as a `condition-name`. `parameter-key` and `parameter-value` are expected to be simple alphanumeric strings with some additional characters like underscores or hyphens.

For example:
 
``` json
 {
     "url": "/myPage/?adminMode={condition.is_admin_mode}",  
     "url2": "/myPage/?adminMode={condition.has_project_permission(permission=BROWSE_PROJECTS)}"
 }
```

The following condition **does not** work as context parameters:

* `user_is_the_logged_in_user` (JIRA)


## <a name="additional-parameters-jira"></a>JIRA context parameters:

 * `issue.id`, `issue.key`, `issuetype.id`
 * `project.id`, `project.key`
 * `version.id`
 * `component.id`
 * `profileUser.name`, `profileUser.key` (available for user profile pages)
 * `dashboardItem.id`, `dashboardItem.key`, `dashboardItem.viewType`, `dashboard.id` (available for dashboard items)

JIRA issue pages only expose issue and project data. Similarly, version and component information is available only in
project administration pages.

### <a name="additional-parameters-jira-service-desk"></a>JIRA Service Desk context parameters

JIRA Service Desk supports these context variables.

 * `servicedesk.requestId` for 'Request details view' 
 * `servicedesk.requestTypeId` for 'Create request view' and 'Request details view'
 * `servicedesk.serviceDeskId` for 'Create request view', 'Request details view', and 'Portal page'

### <a name="additional-parameters-jira-software"></a>JIRA Software context parameters

JIRA Software supports these context variables.

 * `board.id`, `board.type`, `board.screen` (for plugin points displayed in multiple board screens), `board.screen` (formerly `board.mode`) 
 * `sprint.id`, `sprint.state` 



## <a name="add-on-specific-context-parameters"></a>Add-on-specific context parameters

Add-ons can define their own context for [pages](/cloud/jira/platform/modules/page/). To do this, add query parameters to the URL of the Atlassian application. 

Use `ac.` as the prefix for each query parameter key.

For example:

``` json
 {
     "url": "/things/{ac.action}/?thingId={ac.thingId}"
 }
```

In the url above, `{ac.action}` and `{ac.thingId}` will be substituted with values of the `ac.action` and
`ac.thingId` parameters that are taken from the Atlassian application's current URL. For example, if the application's
current URL is `http://example.atlassian.net/plugins/servlet/ac/add-on-key/general-page-key?ac.action=edit&ac.thingId=12`
then the resulting add-on URL will be `/things/edit/?thingId=12`.

If there are multiple query parameters with the same name in the Atlassian application URL,
only the value of the first occurrence will be used.