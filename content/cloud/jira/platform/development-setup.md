---
title: Development Setup
platform: cloud
product: jiracloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/development-setup.html
- /jiracloud/development-setup.md
date: "2016-10-04"

---
{{< include path="docs/content/cloud/connect/tasks/development-setup.snippet.md">}}
