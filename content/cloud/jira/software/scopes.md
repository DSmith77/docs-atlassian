---
title: Scopes
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
aliases:
- /cloud/jira/software/scopes.html
- /cloud/jira/software/scopes.md
date: "2016-10-31"
---
{{< include path="docs/content/cloud/connect/reference/jira-scopes.snippet.md">}}