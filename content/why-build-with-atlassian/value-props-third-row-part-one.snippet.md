<h4 class="center">We provide the cloud environment</h4>

How about a sandbox with 5 free users? Just head on over to <a href="https://www.atlassian.com/ondemand/signup/form?product=confluence.ondemand,jira-software.ondemand,jira-servicedesk.ondemand,jira-core.ondemand&developer=true">go.atlassian.com/cloud-dev</a> to sign up for a free instance of our flagship products.
