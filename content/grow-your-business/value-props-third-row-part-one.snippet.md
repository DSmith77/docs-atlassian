<h2 class="title">Wittified</h2>

“I had written a plugin that integrates Chef (a popular configuration management solution) with Atlassian Bamboo… and realized that I could make a business out of developing add-ons,” says Wittified cofounder Daniel Wester. He got the idea while working for an enterprise organization and has since built a thriving software company on the Atlassian Marketplace.

<a href="/showcase/wittified/">Learn more<i class="fa fa-arrow-right" aria-hidden="true"></i></a>